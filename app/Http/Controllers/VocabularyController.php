<?php
namespace App\Http\Controllers;

use App\Algorithm;
use App\Vocabulary;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Route;
use Request;
use Response;

class VocabularyController extends Controller
{
    /**
     * Show all words and algorithms
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $algorithms = Algorithm::all()->sortBy('name');
        $words = Vocabulary::all()->sortByDesc('id');
        return view('vocabulary', compact(['words','algorithms']));
    }

    /**
     * Save new word to the vocabulary
     * @return bool
     */
    public function store()
    {
        if (Request::ajax()){
            $data = Input::all();
            if (isset($data['word'])){
                $data['word'] = strip_tags(trim($data['word']));
            }
            //Validate word before save
            $validator = Validator::make($data, ['word' => 'required|string']);
            if ($validator->passes()) {
                $vocabulary = new Vocabulary();
                $vocabulary->word = $data['word'];
                $vocabulary->save();

                $result['status'] = 'success';
                $result['word'] = $vocabulary;
            } else {
                $result['status'] = 'error';
                //return errors if validate false
                $result['errors'] = $validator->errors();
            }
            return Response::json($result);
        }

        return false;
    }

    /**
     * Delete word from vocabulary
     * @param Route $route
     * @return bool
     */
    public function destroy(Route $route)
    {
        if (Request::ajax()){
            $wordId = $route->getParameter('vocabulary', false);
            $wordModel = Vocabulary::where('id', $wordId)->first();
            if ($wordModel){
                $wordModel->delete();
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
            }
            return Response::json($result);
        }
        return false;
    }
}