<?php
namespace App\Http\Controllers;

use App\Algorithm;
use App\Hash;
use App\Vocabulary;
use App\Helpers\Geo;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Route;
use Request;
use Response;
use Illuminate\Support\Facades\Cookie;

class HashController extends Controller
{
    /**
     * Show all saved hashes
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $hashes = Hash::all();
        return view('savedHashes', compact('hashes'));
    }

    /**
     * Receive word ID and algorithm ID. Return hashes.
     * @return bool|Response
     */
    public function generateHash()
    {
        if (Request::ajax()){
            $data = Input::all();
            if (isset($data['algorithms']) && isset($data['words'])){
                $algoIdArr = $data['algorithms'];
                $wordIdArr = $data['words'];
                $algorithms = Algorithm::whereIn('id', $algoIdArr)->get();
                $words = Vocabulary::whereIn('id', $wordIdArr)->get();

                if (count($algorithms) && count($words)){
                    foreach ($algorithms as $algorithm){
                        $wordsWithHash = [];
                        foreach ($words as $word){
                            try {
                                $clonedWord = clone($word);
                                //add hash to the word object
                                $clonedWord->hash = hash($algorithm->name, $clonedWord->word, false);
                                $wordsWithHash[] = $clonedWord;
                            } catch (\ErrorException $e){}
                        }
                        if (isset($wordsWithHash)){
                            //add array words array to the algorithm object
                            $algorithm->wordsWithHash = $wordsWithHash;
                        }
                    }
                    //return popup with hashes
                    $view = view('generatedHashes', ['algorithms' => $algorithms]);
                    return response($view);
                } else {
                    $result['status'] = 'error';
                }
            } else {
                $result['status'] = 'error';
            }
            return Response::json($result);
        }
        return false;
    }

    /**
     * Save new hash to the hash table
     * @return bool
     */
    public function store()
    {
        if (Request::ajax()){
            $data = Input::all();
            $validator = Validator::make($data, [
                'wordId' => 'required|integer',
                'algoId' => 'required|integer'
            ]);
            if ($validator->passes()) {
                $word = Vocabulary::where('id', $data['wordId'])->first();
                $algorithm = Algorithm::where('id', $data['algoId'])->first();
                if ($word && $algorithm){
                    try {
                        $hash = hash($algorithm->name, $word->word, false);
                        $ip = Geo::getIp();
                        $geoData = Geo::getGeoData($ip);

                        $hashModel = new Hash();
                        $hashModel->word = $word->word;
                        $hashModel->algorithm = $algorithm->name;
                        $hashModel->hash_value = $hash;
                        $hashModel->ip = $ip;
                        $hashModel->browser = $_SERVER['HTTP_USER_AGENT'];
                        $hashModel->cookie = serialize(Cookie::get());
                        $hashModel->country = isset($geoData['ip']['country']) ? $geoData['ip']['country'] : false;

                        $hashModel->save();

                        $result['status'] = 'success';
                    } catch (\ErrorException $e){
                        $result['status'] = 'error';
                    }
                } else {
                    $result['status'] = 'error';
                }
            } else {
                $result['status'] = 'error';
                $result['errors'] = $validator->errors();
            }
            return Response::json($result);
        }
        return false;
    }

    /**
     * Delete saved hash from database
     * @param Route $route
     * @return bool
     */
    public function destroy(Route $route)
    {
        if (Request::ajax()){
            $hashId = $route->getParameter('hash', false);
            $hashModel = Hash::where('id', $hashId)->first();
            if ($hashModel){
                $hashModel->delete();
                $result['status'] = 'success';
            } else {
                $result['status'] = 'error';
            }
            return Response::json($result);
        }
        return false;
    }
}