<?php

namespace App\Helpers;

use Ixudra\Curl\Facades\Curl;
use SoapBox\Formatter\Formatter;

class Geo
{
    /**
     * Return information about user's IP
     * @param bool $ip
     * @return bool
     */
    public static function getGeoData($ip = false)
    {
        $geoArr = false;
        if ($ip) {
            $data = Curl::to('http://ipgeobase.ru:7020/geo?ip=' . $ip)->get();
            $formatter = Formatter::make($data, Formatter::XML); // кодировка сама береться
            $geoArr = $formatter->toArray();
        }

        return $geoArr;
    }

    /**
     * Return user's IP or false
     * @return bool|string
     */
    public static function getIp()
    {

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
            return $ip;
        }

        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) { // proxy
            $ip = trim(strtok($_SERVER['HTTP_X_FORWARDED_FOR'], ','));
            return $ip;
        }

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
            return $ip;
        }
        if (isset($_SERVER['HTTP_X_REAL_IP'])) {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
            return $ip;
        }

        return false;
    }
}