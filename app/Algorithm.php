<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Algorithm extends Model
{
    protected $table = 'algorithm';
    public $timestamps = false;
}
