<?php

namespace App\Console\Commands;

use App\Hash;
use App\Vocabulary;
use Illuminate\Console\Command;
use SoapBox\Formatter\Formatter;
use Storage;

class MakeXml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xml:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create xml files with hashes information about users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hashes = Hash::all();
        if (count($hashes)){
            foreach ($hashes as $hash){
                //take similar words, excluding the same word
                //May use MATCH to find similar if table engine MyISAM and field 'word' has FULLTEXT index
                $similar = Vocabulary::where('word', 'LIKE', '%'.$hash->word.'%')
                    ->where('word', '!=', $hash->word)
                    ->pluck('word');
                if (count($similar)){
                    $hash->similar = $similar;
                }
            }
            $hashes = $hashes->toArray();
            $formatter = Formatter::make($hashes, Formatter::ARR);
            $xml = $formatter->toXml();
            Storage::disk('local')->put(date('YmdHi').'hash.xml', $xml);
        }
    }
}
