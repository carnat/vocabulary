$(document).ready(function () {
    //Add CSRF token to ajax
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Remove error border from word input
    $('#new-word-input').on('focus', function () {
        $(this).removeClass('error');
    });

    //Send new word to save. Add new word to list if save success.
    $('#js-add-word').click(function (e) {
        e.preventDefault();
        var wordInput = $(this).parents('.new-word-form').find('#new-word-input');
        var word = wordInput.val().trim();
        if (word){
            $.ajax({
                url: '/vocabulary',
                type: 'POST',
                data: {'word': word},
                success: function(data) {
                    if (data.status == 'success'){
                        wordInput.removeClass('error');
                        wordInput.val('');
                        $('.words').prepend('<div class="word-block"><input type="checkbox" name="'+ data.word.id +'"><span>'+ data.word.word +'</span><span class="js-del-word"><img class="icon icon-delete" src="/img/delete.png" alt="delete-icon"></span>');
                    } else {
                        wordInput.addClass('error');
                    }
                }
            });
        } else {
            wordInput.addClass('error');
        }
    });

    //Remove word from vocabulary
    $(document).on('click', '.js-del-word', function (e) {
        e.preventDefault();
        var parent = $(this).parents('.word-block');
        var wordId = parent.find('input').attr('name');
        $.ajax({
            type: 'DELETE',
            url: '/vocabulary/'+wordId,
            success: function (data) {
                if (data.status == 'success'){
                    parent.remove();
                }
            }
        });
    });

    //Collect and send words and algorithms. Show hashes popup if success.
    $('#js-generate-hash').click(function (e) {
        e.preventDefault();
        $('.error-message').removeClass('active');
        $('.generated-hashes').remove();
        var algorithms = [];
        var words = [];
        var message;
        $('.algorithms input:checked').each(function (key, elt) {
            algorithms.push($(elt).attr('name'));
        });
        $('.words input:checked').each(function (key, elt) {
            words.push($(elt).attr('name'));
        });
        if (!algorithms.length || !words.length){
            if (!algorithms.length){
                message = 'Select algorithm';
            } else {
                message = 'Select word';
            }
            $('.error-message').addClass('active');
            $('.error-message .error-text').text(message);
        } else {
            $.post('/hash/generate', {
                'algorithms': algorithms,
                'words': words
            }, function (data) {
                if (data.status !== 'error'){
                    $('.popup-overlay').addClass('active');
                    $('.generated-hashes-block').append(data);
                }
            })
        }
    });

    //Close popup
    $(document).on('click', '.popup-overlay, .close-icon', function () {
        $('.popup-overlay').removeClass('active');
        $('.generated-hashes').remove();
    });

    //Send word and algorithm ID to create hash and save it
    $(document).on('click', '.js-save-hash', function () {
        var that = $(this);
        var algoId = $(this).parents('.algorithm-item').attr('data-id');
        var wordId = $(this).parents('.word-item').attr('data-id');
        if (algoId > 0 && wordId > 0){
            $.ajax({
                url: '/hash',
                type: 'POST',
                data: {
                    'algoId': algoId,
                    'wordId': wordId
                },
                success: function(data) {
                    if (data.status == 'success'){
                        that.removeClass('.js-save-hash');
                        that.find('img').removeClass('icon').attr('src', '/img/check.png');
                    }
                }
            });
        }
    });

    //Delete saved hash from database
    $('.js-del-hash').click(function () {
        var parentBlock = $(this).parents('.hash-item');
        var hashId = $(this).attr('data-id');
        $.ajax({
            type: 'DELETE',
            url: '/hash/'+hashId,
            success: function (data) {
                if (data.status == 'success'){
                    parentBlock.remove();
                }
            }
        });
    });
});