@extends('layouts.app')

@section('content')
    <h2>What You should do?</h2>
    <ul>
        <li>Click "Create Hash" link.</li>
        <li>Create several new words.</li>
        <li>Select (one or more) algorithm and word.</li>
        <li>Press "Generate hash" button.</li>
        <li>In popup you can save hash to the database.</li>
        <li>Click "Saved hashes" to access saved hashes.</li>
        <li>To take xml files with information about user and hash, you need to add the string "* * * * * php /path/to/artisan schedule:run" to Cron.<br>
            The following method would store a files in /path/to/project/storage/app/.
        </li>
    </ul>
@endsection