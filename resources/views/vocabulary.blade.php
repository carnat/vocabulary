@extends('layouts.app')

@section('content')
    <div class="error-message"><span class="error-text"></span></div>
    <div class="algorithms">
        <div class="algorithms-header">Algorithm</div>
        @foreach ($algorithms as $algo)
            <input type="checkbox" name="{{$algo->id}}"><span>{{$algo->name}}</span>
        @endforeach
        @if (count($algorithms))
            <button id="js-generate-hash">Generate hash</button>
        @endif
    </div>
    <form class="new-word-form" method="POST">
        <input type="text" id="new-word-input" name="word" placeholder="New word">
        <button id="js-add-word">Add new word</button>
    </form>
    <form class="words">
        @foreach($words as $word)
            <div class="word-block">
                <input type="checkbox" name="{{$word->id}}">
                <span>{{$word->word}}</span>
                <span class="js-del-word">
                    <img class="icon icon-delete" src="{{asset('img/delete.png')}}" alt="delete-icon">
                </span>
            </div>
        @endforeach
    </form>
    <div class="generated-hashes-block">

    </div>
@endsection