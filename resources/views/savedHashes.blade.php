@extends('layouts.app')

@section('content')
    <table class="hash-table">
        <tr>
            <th>Word</th>
            <th>Algorithm</th>
            <th>Hash</th>
            <th>IP</th>
            <th>Country</th>
            <th>Date added</th>
            <th>Del</th>
        </tr>
        @foreach ($hashes as $hash)
            <tr class="hash-item">
                <td>{{$hash->word}}</td>
                <td>{{$hash->algorithm}}</td>
                <td>{{$hash->hash_value}}</td>
                <td>{{$hash->ip}}</td>
                <td>{{$hash->country}}</td>
                <td>{{$hash->date_added}}</td>
                <td><img data-id="{{$hash->id}}" class="icon icon-delete js-del-hash" src="{{asset('img/delete.png')}}" alt="delete-icon"></td>
            </tr>
        @endforeach
    </table>
@endsection