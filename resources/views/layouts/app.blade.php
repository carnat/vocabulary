<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <title>Vocabulary</title>

</head>
<body>
<div class="popup-overlay"></div>
<h1 class="main-header">Hash generator</h1>
<div class="topmenu">
    <div class="menu-link"><a href="/">Guide</a></div>
    <div class="menu-link"><a href="/vocabulary/">Create hash</a></div>
    <div class="menu-link"><a href="/hash/">Saved hashes</a></div>
</div>
<div>
    @yield('content')
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="{{ asset('js/helper.js') }}"></script>
</body>
</html>
