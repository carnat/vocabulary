<div class="generated-hashes">
    <img class="icon close-icon" src="{{asset('img/close.png')}}" alt="close-icon">
    @foreach ($algorithms as $algorithm)
        <div class="algorithm-item" data-id="{{$algorithm->id}}">
            <span class="algorithm-name">{{$algorithm->name}}</span>
            @if (isset($algorithm->wordsWithHash))
                @foreach ($algorithm->wordsWithHash as $word)
                    <div class="word-item" data-id="{{$word->id}}">
                        <span>{{$word->word}} : {{$word->hash}}</span>
                        <span class="js-save-hash">
                            <img class="icon icon-save" src="{{asset('img/save.png')}}" alt="save-icon">
                        </span>
                    </div>
                 @endforeach
             @endif
         </div>
    @endforeach
</div>