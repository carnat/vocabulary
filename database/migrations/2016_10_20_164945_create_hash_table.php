<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHashTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('hash')) {
            Schema::create('hash', function (Blueprint $table) {
                $table->increments('id');
                $table->string('word');
                $table->string('algorithm');
                $table->string('hash_value');
                $table->string('ip');
                $table->string('browser');
                $table->string('country');
                $table->text('cookie');
                $table->dateTime('date_added')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('hash')) {
            Schema::drop('hash');
        }
    }
}
