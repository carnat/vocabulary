<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillAlgorithmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('algorithm')) {
            DB::table('algorithm')->insert([
                'name' => 'sha1'
            ]);
            DB::table('algorithm')->insert([
                'name' => 'sha256'
            ]);
            DB::table('algorithm')->insert([
                'name' => 'md5'
            ]);
            DB::table('algorithm')->insert([
                'name' => 'ripemd128'
            ]);
            DB::table('algorithm')->insert([
                'name' => 'ripemd256'
            ]);
            DB::table('algorithm')->insert([
                'name' => 'md4'
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
